
public class Polynom {

	private int coefficients;
	
	public Polynom(int coef) {
		coefficients = coef;
	}
	
	public static Polynom irred() {
		return new Polynom(0b100011011);
	}
	
	private Polynom add(Polynom b) {
		coefficients ^= b.coefficients;
		return this;
	}
	
	private Polynom mult(Polynom b) {
		Polynom tmp = new Polynom(0);
		for(int i=0; i<31; i++)
			if(((int)Math.pow(2,i) & b.coefficients) != 0)
				tmp.add(new Polynom(coefficients << i));
		tmp = tmp.divide(irred(), new Polynom(0));
		return tmp;
	}
	
	private int getHighestIndex() {
		for(int i=30; i>=0; i--)
			if(((int)Math.pow(2,i) & coefficients) != 0)
				return i;
		return -1;
	}
	
	private Polynom divide(Polynom b, Polynom div) {
		if(coefficients == 0) return this;
		if(b.coefficients == 0 || div.coefficients != 0) {
			System.out.println("Fehler");
			return null;
		}
		Polynom a = new Polynom(coefficients);
		int i,j;
		while((i = a.getHighestIndex()) >= (j = b.getHighestIndex())) {
			div.add(new Polynom(0b1 << (i-j)));
			a.add(new Polynom(b.coefficients << (i-j)));
		}
		return a;
	}
	
	private Polynom inverse() {
		Polynom s = irred();
		Polynom t = new Polynom(coefficients);
		Polynom u1 = new Polynom(0b1);
		Polynom u2 = new Polynom(0b0);
		Polynom u = new Polynom(0b0);
		Polynom v1 = new Polynom(0b0);
		Polynom v2 = new Polynom(0b1);
		Polynom v = new Polynom(0b1);
		while(t.coefficients != 0) {
			Polynom div = new Polynom(0);
			Polynom mod = s.divide(t,div);
			if(mod.coefficients == 0)
				break;
			u = u1.add(div.mult(u2));
			v = v1.add(div.mult(v2));
			u1 = u2; u2 = u;
			v1 = v2; v2 = v;
			s = t; t = mod;
		}
		return v;
	}
	
	private boolean equals(Polynom b) {
		return coefficients == b.coefficients;
	}
	
	private static int max() {
		return 0b11111111;
	}
	
	public static void printSelfInverses() {
		for(int i=1; i<max(); i++) {
			Polynom test = new Polynom(i);
			if(test.equals(test.inverse()))
				System.out.println("Selbstinverses: " + Integer.toBinaryString(i));
		}
	}
	
	public static void printInversesWithDiff1() {
		for(int i=1; i<max(); i++) {
			Polynom test = new Polynom(i);
			if(test.equals(test.inverse().add(new Polynom(1))))
				System.out.println("Differenz 1: " + Integer.toBinaryString(i) 
						+ " - " + Integer.toBinaryString(test.inverse().coefficients));
		}
	}

	public static void main(String[] args) {
		printSelfInverses();
		printInversesWithDiff1();
	}
}
