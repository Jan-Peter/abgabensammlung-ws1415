der koch in einem restaurant arbeitet schlampig. wenn
er einen stapel pfannkuchen baeckt, sind alle untersch-
iedlich gross. der kellner sortiert sie dann auf dem weg
zum tisch des gastes (sodass der kleinste am schluss oben
liegt, der naechstgroessere darunter und der groesste ganz
unten). dazu nimmt er mehrere pfannkuchen von oben und
dreht sie um und wiederholt das (mit jeweils unterschied-
lich vielen pfannkuchen) so oft wie noetig. wie oft muss
er n pfannkuchen maximal wenden, bis sie sortiert sind?
