package letterCount;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class LetterCount {
	ArrayList<Character> text;

	public LetterCount(ArrayList<Character> text) {
		this.text =text;
	}

	public HashMap<Character, Integer> countChars() {
		HashMap<Character, Integer> map = new HashMap<Character, Integer>();
		for (Character l : text) {
			if (!map.containsKey(l))
				map.put(l, 1);
			else
				map.put(l, map.get(l) + 1);
		}
		return map;
	}

	public HashMap<Character, Double> letterFrequency() {
		HashMap<Character, Double> frequency = new HashMap<Character, Double>();
		HashMap<Character, Integer> characters = countChars();
		java.util.Iterator<Entry<Character, Integer>> it = characters
				.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Character, Integer> current = it.next();
			frequency.put((Character) current.getKey(),
					(double) current.getValue() / text.size());
			it.remove();
		}

		return frequency;
	}
}
