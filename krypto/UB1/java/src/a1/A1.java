package a1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import letterCount.*;

public class A1 {

	public static void main(String[] args) {
		ArrayList<Character> text = new ArrayList<Character>();
		try {
			BufferedReader in = new BufferedReader(new FileReader("A1.txt"));
			try {
				while (true) {
					int c=in.read();
					if(c==-1)
						break;
					text.add(new Character((char) c));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		LetterCount lc = new LetterCount(text);

		HashMap<Character, Double> frequencys = lc.letterFrequency();
		Iterator<Entry<Character, Double>> it = frequencys.entrySet()
				.iterator();

		while (it.hasNext()) {
			Entry<Character, Double> current = it.next();
			System.out.println(current.getKey() + " : " + current.getValue());
		}
	}

}
