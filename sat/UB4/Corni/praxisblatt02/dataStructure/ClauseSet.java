package praxisblatt02.dataStructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import praxisblatt02.parser.DIMACSReader;

/**
 * A set of clauses.
 * 
 */
public class ClauseSet {
	/* Number of variables */
	private int varNum;

	/* Clauses of this set */
	private Vector<Clause> clauses;

	/* List of all variables */
	private HashMap<Integer, Variable> variables;

	/**
	 * Constructs a clause set from the given DIMACS file.
	 * 
	 * @param filePath
	 *            file path of the DIMACS file.
	 */
	public ClauseSet(String filePath) {
		// TODO: to implement!
		DIMACSReader parser = new DIMACSReader(filePath);
		varNum = parser.getNumVars();
		int[][] cl = parser.getClauses();
		
		variables = new HashMap<Integer, Variable>();
		for(int i=0; i<cl.length; i++)
			for(int j=0; j<cl[i].length; j++)
				if(!variables.containsKey(Math.abs(cl[i][j])))
					variables.put(Math.abs(cl[i][j]), 
							new Variable(Math.abs(cl[i][j])));
		//System.out.println(variables);
		//for(int i=0; i<vars.size(); i++)
		//	variables.put(vars.get(i), new Variable(vars.get(i)));
		//System.out.println("here");
		//for(int i=1; i<=this.varNum; i++)
		//	variables.put(i, new Variable(i));
		
		clauses = new Vector<Clause>();
		for(int i=0; i<cl.length; i++) {
			Vector<Integer> lit = new Vector<Integer>();
			for(int j=0; j<cl[i].length; j++)
				lit.add(cl[i][j]);
			clauses.add(new Clause(lit, variables));
		}
		//System.out.println(clauses);
		//System.out.println("print: ");
		//System.out.println(this);
		
		//System.out.println("beg");
		for(int i=1; i<=this.varNum; i++)
			variables.get(i).computeAdjacencyList(clauses);
		//System.out.println("fin");
	}

	/**
	 * Executes unit propagation and checks for the existence of an empty
	 * clause.
	 * 
	 * @return true, if an empty clause exists, otherwise false.
	 */
	public boolean unitPropagation() {
		// TODO: to implement!
		while (nextUnit() != null) {
			Clause c = nextUnit();
			//System.out.println(c);
			int var = c.getUnassigned(variables);
			//System.out.println(var);
			if(var == 0)
				continue;
			Variable v = variables.get(var);
			v.assign(c.getPolarity(var));
			//c.setSat(true);
			System.out.println(var + " -> " + c.getPolarity(var));
			//System.out.println(c);
		}
		
		// Test
		boolean decided = true;
		for(int i=0; i<clauses.size(); i++)
			if(!clauses.get(i).isSat()) {
				decided = false;
				break;
			}
		System.out.println("containsEmpty: " + containsEmpty());
		System.out.println("sat: " + (containsEmpty() ? "unsat" : 
			(decided ? "sat" : "not decided yet")));
		
		return containsEmpty();
	}

	/**
	 * Returns the next unit clause, if one exists.
	 * 
	 * @return next unit clause, if one exists, otherwise null
	 */
	private Clause nextUnit() {
		// TODO: to implement!
		for (Clause c : clauses)
			if (c.isUnit())
				return c;
		return null;
	}

	/**
	 * Checks, if an empty clause exists.
	 * 
	 * @return true, if an empty clause exists, otherwise false.
	 */
	private boolean containsEmpty() {
		// TODO: to implement!
		for (Clause c : clauses)
			if (c.isEmpty())
				return true;
		return false;
	}

	@Override
	public String toString() {
		return clausesToString() + "\n\n" + varsToString();
	}

	/**
	 * Returns all clauses as string representation.
	 * 
	 * @return a string representation of all clauses.
	 */
	public String clausesToString() {
		String res = "";
		for (Clause clause : clauses)
			res += clause + "\n";
		return res;
	}

	/**
	 * Returns all variables as string representation.
	 * 
	 * @return a string representation of all variables.
	 */
	public String varsToString() {
		String res = "";
		for (int i = 1; i <= varNum; i++)
			res += "Variable " + i + ": " + variables.get(i) + "\n\n";
		return res;
	}
}