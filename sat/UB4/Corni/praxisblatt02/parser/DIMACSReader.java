package praxisblatt02.parser;

import java.io.*;
import java.util.ArrayList;
import praxisblatt02.dataStructure.ClauseSet;

public class DIMACSReader {
	private String filename;
	private String[] problemLine;
	private int numClauses;
	private int numVars;
	private int[][] clauses;
	private int maximumNoOfOccurances;
	private ArrayList<Integer> maximumOccuringVars;

	/**
	 * constructor of a Parser which parses a given file
	 * 
	 * @param file
	 *            : filename of the file to parse
	 */
	public DIMACSReader(String file) {
		this.filename = file;
		this.problemLine = new String[0];

		this.readFile();
	}

	/**
	 * reads the file and separates it into the problem line and the clauses
	 */
	private void readFile() {
		try {
			FileReader fr = new FileReader(filename);
			BufferedReader br = new BufferedReader(fr);

			String line = "";
			do {
				line = br.readLine();
			} while (line.charAt(0) == 'c');
			problemLine = line.split("\\s");

			this.numClauses = Integer.parseInt(problemLine[3]);
			this.numVars = Integer.parseInt(problemLine[2]);
			//this.clauses = new int[numClauses][];

			//int c = 0;

			ArrayList<ArrayList<Integer>> cl = new ArrayList<ArrayList<Integer>>();
			while ((line = br.readLine()) != null) {
				String[] words = line.split("\\s+");
				//clauses[c] = new int[words.length - 1];
				ArrayList<Integer> cls = new ArrayList<Integer>();
				for (int i = 0; i < words.length - 1; i++) {
					if(words[i].equals("\\s+") || words[i].equals(""))
						continue;
					try {
						cls.add(Integer.parseInt(words[i]));
					} catch (Exception e) {
						System.out.println("Fehler: " + e);
					}
					/*try{
						clauses[c][i] = Integer.parseInt(words[i]);
					}catch (Exception e){
						System.out.println("cnf-file is broken; header seems to be not conform with DIMACS-form");
					}*/
				}
				cl.add(cls);
				//c++; // move on to next clause
			}

			this.clauses = new int[cl.size()][];
			for(int i=0; i<cl.size(); i++) {
				clauses[i] = new int[cl.get(i).size()];
				for(int j=0; j<cl.get(i).size(); j++)
					clauses[i][j] = cl.get(i).get(j);
			}

			cl.clear();
			br.close();

			getMaximumOccurrences();
		} catch (Exception e) {
			System.out.println("Error while reading " + filename
					+ "! Exception was: " + e.getMessage());
		}
	}

	/**
	 * returns the number of variables as defined in the problem line
	 * 
	 * @return number of variables
	 */
	public int stateVars() {
		return Integer.parseInt(problemLine[2]);
	}

	/**
	 * returns the number of clauses as defined in the problem line
	 * 
	 * @return number of clauses
	 */
	public int stateClauses() {
		return Integer.parseInt(problemLine[3]);
	}

	/**
	 * returns the count of variables in the set of clauses
	 * 
	 * @return number of variables
	 */
	private int countVars() {
		// return numVars;
		// bzw. wenn man der Problemzeile nicht trauen moechte:
		ArrayList<Integer> list = new ArrayList<Integer>();
		int c=0;
		for(int i=0; i<clauses.length; i++) {
			for(int j=0; j<clauses[i].length; j++) {
				int elem = clauses[i][j];
				if(!list.contains(Math.abs(elem))) {
					c++;
					list.add(Math.abs(elem));
				}
			}
		}
		return c;
	}

	/**
	 * returns the count of clauses
	 * 
	 * @return number of clauses
	 */
	private int countClauses() {
		return numClauses;
	}


	/**
	 * counts the number of literals in the set of clauses
	 * @return number of literals
	 */
	private int countLiterals() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		int c=0;
		for(int i=0; i<clauses.length; i++) {
			for(int j=0; j<clauses[i].length; j++) {
				int elem = clauses[i][j];
				if(!list.contains(elem)) {
					c++;
					list.add(elem);
				}
			}
		}
		return c;
	}

	/**
	 * counts the occurrences of all variables
	 */
	private void getMaximumOccurrences() {
		int[] countOccurrences = new int[numVars];
		for (int i = 0; i < clauses.length; i++) {
			for (int j = 0; j < clauses[i].length; j++) {
				int elem = clauses[i][j];
				if (elem < 0)
					elem *= -1;
				countOccurrences[elem - 1]++;
			}
		}
		int max = Integer.MIN_VALUE;
		ArrayList<Integer> maxVar = new ArrayList<Integer>();
		for (int i = 0; i < countOccurrences.length; i++) {
			if (countOccurrences[i] > max) {
				max = countOccurrences[i];
				maxVar.clear();
				maxVar.add(i + 1);
			} else if (countOccurrences[i] == max) {
				maxVar.add(i + 1);
			}
		}
		maximumNoOfOccurances = max;
		maximumOccuringVars = maxVar;
	}

	/**
	 * collects pure positive literals in the clauses
	 * 
	 * @return set of pure positive literals
	 */
	private ArrayList<Integer> getPositivePureLiterals() {
		boolean[] negatives = new boolean[numVars + 1];
		for (int i = 0; i < clauses.length; i++) {
			for (int j = 0; j < clauses[i].length; j++) {
				int elem = clauses[i][j];
				if (elem < 0) {
					negatives[elem * (-1)] = true;
				}
			}
		}
		ArrayList<Integer> pos = new ArrayList<Integer>();
		for (int i = 1; i < negatives.length; i++)
			if (!negatives[i]) {
				pos.add(new Integer(i));
			}
		return pos;
	}

	/**
	 * collects pure negative literals in the clauses
	 * 
	 * @return set of pure negative literals
	 */
	private ArrayList<Integer> getNegativePureLiterals() {
		boolean[] positives = new boolean[numVars + 1];
		for (int i = 0; i < clauses.length; i++) {
			for (int j = 0; j < clauses[i].length; j++) {
				int elem = clauses[i][j];
				if (elem > 0) {
					positives[elem] = true;
				}
			}
		}
		ArrayList<Integer> neg = new ArrayList<Integer>();

		for (int i = 1; i < positives.length; i++)
			if (!positives[i]) {
				neg.add(new Integer(i));
			}
		return neg;
	}

	/**
	 * collects unit clauses
	 * 
	 * @return set of unit clauses
	 */
	public ArrayList<Integer> getUnitClauses() {
		ArrayList<Integer> units = new ArrayList<Integer>();
		for (int i = 0; i < clauses.length; i++)
			if (clauses[i].length == 1)
				units.add(clauses[i][0]);
		return units;
	}

	/**
	 * prints information about the given file to the terminal
	 */
	private void printInformation() {
		System.out.println("File: " + filename);
		System.out.println("Problem line: #vars = " + stateVars()
				+ ", #clauses = " + stateClauses()); 
		System.out.println("Variable count: " + countVars());
		System.out.println("Clause count: " + countClauses());
		System.out.println("Literal count: " + countLiterals());
		System.out.println("Maximal occurrences of a variable: "
				+ maximumNoOfOccurances);
		System.out.println("Variables with maximum number of occurrences: "
				+ maximumOccuringVars);
		System.out.println("Positive pure literals: "
				+ getPositivePureLiterals());
		System.out.println("Negative pure literals: "
				+ getNegativePureLiterals());
		System.out.println("Unit clauses: " + getUnitClauses());
	}

	/**
	 * @return filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @return number of clauses
	 */
	public int getNumClauses() {
		return numClauses;
	}

	/**
	 * @return number of used variables
	 */
	public int getNumVars() {
		return numVars;
	}


	/**
	 * @return highest number of occurrences for one variable
	 */
	public int getMaximumNoOfOccurances() {
		return maximumNoOfOccurances;
	}

	/**
	 * @return the variable(s) with the highest occurrence
	 */
	public ArrayList<Integer> getMaximumOccuringVar() {
		return maximumOccuringVars;
	}
	
	/**
	 * @return the clauses
	 */
	public int[][] getClauses() {
		return clauses;
	}

	public static void main(String[] args) throws IOException {
		String file = "miza-sr06-md5-47-03.cnf";//"hole8-no.cnf";
		//DIMACSReader r = new DIMACSReader(file);
		//r.printInformation();
		
		ClauseSet cl = new ClauseSet(file);
		//System.out.println(cl);
		boolean up = cl.unitPropagation();
		System.out.println(file + " - unsat proofed by UP: " + up);
	}
}
