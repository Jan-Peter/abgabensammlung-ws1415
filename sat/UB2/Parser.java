import java.io.*;
import java.util.ArrayList;

public class Parser {
	private String filename;
	private String[] problemLine;
	private ArrayList<ArrayList<String>> clauses;
	int vars;
	int literals;
	int[] occurrences;
	int maximum;

	/**
	 * constructor of a Parser which parses a given file
	 * @param file: filename of the file to parse
	 */
	public Parser(String file) {
		this.filename = file;
		this.problemLine = new String[0];
		this.clauses = new ArrayList<ArrayList<String>>();

		this.readFile();
	}

	/**
	 * reads the file and separates it into the problem line and the clauses
	 */
	private void readFile() {
		try {
			FileReader fr = new FileReader(filename);
			BufferedReader br = new BufferedReader(fr);

			String line = "";
			do {
				line = br.readLine();
			} while(line.charAt(0) == 'c');
			problemLine = line.split("\\s");

			while((line = br.readLine()) != null) {
				String[] words = line.split("\\s");
				ArrayList<String> list = new ArrayList<String>();
				for(int i=0; i<words.length-1; i++)
					list.add(words[i]);
				clauses.add(list);
			}

			br.close();
		} catch (Exception e) {
			System.out.println("Error while reading " +  filename + 
					"! Exception was: " + e.getMessage());
		}	
	}

	/**
	 * returns the number of variables as defined in the problem line
	 * @return number of variables
	 */
	private int stateVars() {
		return Integer.parseInt(problemLine[2]);
	}

	/**
	 * returns the number of clauses as defined in the problem line
	 * @return number of clauses
	 */
	private int stateClauses() {
		return Integer.parseInt(problemLine[3]);
	}
	
	/**
	 * counts the number of variables in the set of clauses
	 * @return number of variables
	 */
	private int countVars() {
		ArrayList<String> list = new ArrayList<String>();
		int c=0;
		for(int i=0; i<clauses.size(); i++) {
			for(int j=0; j<clauses.get(i).size(); j++) {
				String elem = clauses.get(i).get(j);
				if(elem.isEmpty())
					continue;
				if(elem.charAt(0) == '-')
					elem = elem.substring(1);
				if(!list.contains(elem)) {
					c++;
					list.add(elem);
				}
			}
		}
		vars = c;
		return c;
	}

	/**
	 * counts the number of clauses
	 * @return number of clauses
	 */
	private int countClauses() {
		return clauses.size();
	}

	/**
	 * counts the number of literals in the set of clauses
	 * @return number of literals
	 */
	private int countLiterals() {
		ArrayList<String> list = new ArrayList<String>();
		int c=0;
		for(int i=0; i<clauses.size(); i++) {
			for(int j=0; j<clauses.get(i).size(); j++) {
				String elem = clauses.get(i).get(j);
				if(elem.isEmpty())
					continue;
				if(!list.contains(elem)) {
					c++;
					list.add(elem);
				}
			}
		}
		return c;
	}

	/**
	 * counts the occurrences of all variables and returns the maximum value
	 * @return maximum occurrence of a variable in the clauses
	 */
	private int getMaximumOccurrences() {
		int[] countOccurrences = new int[vars];
		for(int i=0; i<clauses.size(); i++) {
			for(int j=0; j<clauses.get(i).size(); j++) {
				String elem = clauses.get(i).get(j);
				if(elem.isEmpty())
					continue;
				if(elem.charAt(0) == '-')
					elem = elem.substring(1);
				countOccurrences[Integer.parseInt(elem)-1]++;
			}
		}
		int max = 0;
		for(int i=0; i<countOccurrences.length; i++) {
			if(countOccurrences[i] > max)
				max = countOccurrences[i];
		}
		occurrences = countOccurrences;
		maximum = max;
		return max;
	}

	/**
	 * returns the variables which have the maximum occurrence as computed above
	 * @return set of variables with maximum occurrence
	 */
	private ArrayList<String> getVariablesWithMaxOccurrences() {
		ArrayList<String> vars = new ArrayList<String>();
		for(int i=0; i<occurrences.length; i++) {
			if(occurrences[i] == maximum)
				vars.add(new Integer(i+1).toString());
		}
		return vars;
	}

	/**
	 * collects pure positive literals in the clauses
	 * @return set of pure positive literals
	 */
	private ArrayList<String> getPositivePureLiterals() {
		boolean[] positives = new boolean[vars];
		for(int i=0; i<clauses.size(); i++) {
			for(int j=0; j<clauses.get(i).size(); j++) {
				String elem = clauses.get(i).get(j);
				if(elem.isEmpty())
					continue;
				if(elem.charAt(0) == '-') {
					elem = elem.substring(1);
					positives[Integer.parseInt(elem)-1] = true;
				}
			}
		}
		ArrayList<String> pos = new ArrayList<String>();
		for(int i=0; i<positives.length; i++)
			if(!positives[i])
				pos.add(new Integer(i+1).toString());
		return pos;
	}

	/**
	 * collects pure negative literals in the clauses
	 * @return set of pure negative literals
	 */
	private ArrayList<String> getNegativePureLiterals() {
		boolean[] negatives = new boolean[vars];
		for(int i=0; i<clauses.size(); i++) {
			for(int j=0; j<clauses.get(i).size(); j++) {
				String elem = clauses.get(i).get(j);
				if(elem.isEmpty())
					continue;
				if(elem.charAt(0) != '-')
					negatives[Integer.parseInt(elem)-1] = true;
			}
		}
		ArrayList<String> neg = new ArrayList<String>();
		for(int i=0; i<negatives.length; i++)
			if(!negatives[i])
				neg.add(new Integer(i+1).toString());
		return neg;
	}

	/**
	 * collects unit clauses
	 * @return set of unit clauses
	 */
	private ArrayList<String> getUnitClauses() {
		ArrayList<String> units = new ArrayList<String>();
		for(int i=0; i<clauses.size(); i++)
			if(clauses.get(i).size() == 1)
				units.add(clauses.get(i).get(0));
		return units;				
	}

	/**
	 * prints information about the given file to the terminal
	 */
	private void printInformation() {
		System.out.println("File: " + filename);
		System.out.println("Problem line: #vars = " + stateVars() + 
				", #clauses = " + stateClauses());
		System.out.println("Variable count: " + countVars());
		System.out.println("Clause count: " + countClauses());
		System.out.println("Literal count: " + countLiterals());
		System.out.println("Maximal occurrences of a variable: " + 
				getMaximumOccurrences());
		System.out.println("Variables with maximum number of occurrences: " + 
				getVariablesWithMaxOccurrences());
		System.out.println("Positive pure literals: " + 
				getPositivePureLiterals());
		System.out.println("Negative pure literals: " + 
				getNegativePureLiterals());
		System.out.println("Unit clauses: " + getUnitClauses());
	}

	public static void main(String[] args) throws IOException{
		String file = "goldb-heqc-k2mul.cnf";
		Parser parse = new Parser(file);
		parse.printInformation();
	}
}
