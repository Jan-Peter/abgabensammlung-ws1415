package ecc;

public class ZpElem {

	public final int v;
	public final Zp zp;

	public ZpElem(int v, Zp zp) {
		this.v = v % zp.getP();
		this.zp = zp;
	}

	public int getValue() {
		return v;
	}

	public ZpElem add(ZpElem y) {
		return zp.add(this, y);
	}

	public ZpElem sub(ZpElem y) {
		return zp.sub(this, y);
	}

	public ZpElem mult(ZpElem y) {
		return zp.mult(this, y);
	}

	public ZpElem mult(int a) {
		return new ZpElem(v * a,zp);
	}

	public ZpElem div(ZpElem y) {
		return zp.div(this, y);
	}

	public ZpElem pow(int i) {
		return zp.pow(this, i);
	}

	public ZpElem getInverse() {
		int i = 0;
		// TODO EEA
		while (i <= zp.getP()) {
			if ((v * i % zp.getP()) == 1) {
				return new ZpElem(i, zp);
			}
			i++;
		}
		return null;
	}

	public ZpElem add(int a) {
		return new ZpElem(v+a, zp);
	}
	
	@Override
	public String toString() {
		return ""+v;
	}

	public ZpElem getAddInverse() {
		return new ZpElem((this.zp.getP()-this.v), this.zp);
	}
}
