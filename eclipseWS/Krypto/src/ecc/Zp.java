package ecc;

import java.util.LinkedList;

public class Zp {

	private final int p;

	public Zp(int p) {
		this.p = p;
	}

	public LinkedList<ZpElem> sqrt(ZpElem d) {
		double e = 0;
		double f = d.getValue();
		LinkedList<ZpElem> sol = null;
		for (int i = 0; i < p; i++) {
			e = Math.sqrt((double) f);
			if (!(e - (int) e < 0.999 && e - (int) e > 0.001)) {
				if (sol == null)
					sol = new LinkedList<ZpElem>();
				sol.add(new ZpElem((int) Math.round(e), d.zp));
			}
			f = f + p;
		}
		return sol;
	}

	public void printG(Curve c) {
		int x = 0;
		ZpElem i = new ZpElem(0, this);
		for (int k = 0; k < p; k++) {
			LinkedList<ZpElem> j = c.evalToY(i);
			String s = (j == null) ? "" : "("
					+ i
					+ "|"
					+ j.get(0)
					+ ")\n"
					+ ((j.get(0).getValue() != 0) ? "(" + i + "|" + j.get(1)
							+ ")\n" : "");
			if (j != null)
				if (j.get(0).getValue() != 0)
					x += 2;
				else
					x++;
			System.out.print(s);
			i = i.add(1);
		}
		System.out.println("fern");
		System.out.println("|G|=" + (x + 1));
	}

	public LinkedList<Point> calcG(Curve c) {
		ZpElem z = new ZpElem(0, this);
		LinkedList<Point> list = new LinkedList<Point>();
		list.add(new Point());
		for (int i = 0; i < p; i++) {
			LinkedList<ZpElem> ys = c.evalToY(z);
			//ys contains 0,1 or 2 values
			if (ys != null) {
				list.add(new Point(z, ys.get(0), c));
				if (ys.get(0).getValue() != 0)
					list.add(new Point(z, ys.get(1), c));
			}
			z = z.add(1);
		}
		return list;
	}

	public int getP() {
		return p;
	}

	public ZpElem add(ZpElem x, ZpElem y) {
		return new ZpElem(x.getValue() + y.getValue(), this);
	}

	public ZpElem sub(ZpElem x, ZpElem y) {
		return new ZpElem(x.getValue() - y.getValue() + p, this);
	}

	public ZpElem mult(ZpElem x, ZpElem y) {
		return new ZpElem(x.getValue() * y.getValue(), this);
	}

	public ZpElem div(ZpElem x, ZpElem y) {
		return mult(x, y.getInverse());
	}

	public ZpElem pow(ZpElem x, int i) {
		ZpElem e = new ZpElem(x.getValue(), this);
		i--;
		while (i > 0) {
			e = e.mult(x);
			i--;
		}
		return e;
	}

}
