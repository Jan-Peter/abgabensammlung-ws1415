package ecc;

import java.util.LinkedList;

public class Curve {
	public final ZpElem a;
	public final ZpElem b;
	public final Zp zp;
	
	/**
	 * Create a new elliptic curve of the form y^2=x^3+ax+b
	 * @param a 
	 * @param b
	 * @param p
	 * @throws InsecureException
	 */
	public Curve(ZpElem a, ZpElem b, int p) throws InsecureException{
		if(a.pow(3).mult(4).add(b.pow(2).mult(27)).getValue()==0){
			throw new InsecureException("change a or b");
		}
		if(p<5){
			throw new InsecureException("change p");
		}
		this.a=a;
		this.b=b;
		this.zp=new Zp(p);		
	}

	public LinkedList<ZpElem> evalToY(ZpElem x) {
		return zp.sqrt(x.pow(3).add(x.mult(a)).add(b));
	}
	
	

}
