package ecc;

import java.util.LinkedList;


public class Main {

	public static void main(String[] args) throws InsecureException, InvalidPointsException {

		Zp z11 = new Zp(11);
		Zp z13 = new Zp(13);
		

		Curve c11=new Curve(new ZpElem(1, z11), new ZpElem(0, z11), 11);
		z11.printG(c11);
		System.out.println();
		Curve c13=new Curve(new ZpElem(1, z13), new ZpElem(0, z13), 13);
		z13.printG(c13);
		
		LinkedList<Point> p11=z11.calcG(c11);

		LinkedList<Point> p13=z13.calcG(c13);
		System.out.println();
		for(int i=0; i<p11.size(); i++){
			LinkedList<Point> a = p11.get(i).order();
			System.out.println(a+"->"+a.size());		
		}
		System.out.println();
		for(int i=0; i<p13.size(); i++){
			LinkedList<Point> a = p13.get(i).order();
			System.out.println(a+"->"+a.size());
		}
		
		
		
	}

}
