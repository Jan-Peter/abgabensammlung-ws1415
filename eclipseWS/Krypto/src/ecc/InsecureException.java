package ecc;

public class InsecureException extends Exception {


	public InsecureException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1354291669167338449L;

}
