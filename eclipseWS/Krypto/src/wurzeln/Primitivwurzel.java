package wurzeln;

import java.util.ArrayList;

public class Primitivwurzel {
	int p;
	
	public Primitivwurzel(int p){
		this.p=p;
	}
	
	public boolean isPrimitivwurzel(int g){
		int a=g;
		for(int i=2; i<p-1;i++){
			a*=g;
			a=a%p;
			if(a==1)
				return false;
		}
		return true;
	}
	
	public ArrayList<Integer> primitivwurzeln(){
		ArrayList<Integer> ps=new ArrayList<Integer>();
		for(int i=2; i<p; i++){
			if(isPrimitivwurzel(i)){
				ps.add(i);
			}
		}
		return ps;
	}
	

}
