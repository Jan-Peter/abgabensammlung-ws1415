package tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Arrays;
import java.util.Stack;
import java.util.Vector;

import org.junit.Test;

import dataStructure.*;
import solver.*;

public class CDCLTest {

	@Test
	public void testResolve() {
		Clause[] cls = new Clause[8];
		cls[0] = new Clause();
		cls[1] = new Clause(1);
		cls[2] = new Clause(-1);
		cls[3] = new Clause(1, -1);
		cls[4] = new Clause(1, 2, 3);
		cls[5] = new Clause(1, 2, -3);
		cls[6] = new Clause(1, -2, -3);
		cls[7] = new Clause(1, 2);

		CDCL solver = new CDCL();
		for (int i = 0; i < cls.length; i++)
			if (i != 3)
				assertTrue(solver.resolveTest(cls[i], cls[i]).getLiterals()
						.equals(cls[i].getLiterals()));

		assertTrue(solver.resolveTest(cls[3], cls[3]).getLiterals()
				.equals(cls[0].getLiterals()));
		assertTrue(solver.resolveTest(cls[3], cls[0]).getLiterals()
				.equals(cls[0].getLiterals()));
		assertTrue(solver.resolveTest(cls[0], cls[3]).getLiterals()
				.equals(cls[0].getLiterals()));

		for (int i = 0; i < cls.length; i++)
			if (i != 3) {
				assertTrue(solver.resolveTest(cls[i], cls[0]).getLiterals()
						.equals(cls[i].getLiterals()));
				assertTrue(solver.resolveTest(cls[0], cls[i]).getLiterals()
						.equals(cls[i].getLiterals()));
			}

		for (int i = 4; i < cls.length; i++) {
			assertTrue(solver.resolveTest(cls[1], cls[i]).getLiterals()
					.equals(cls[i].getLiterals()));
			assertTrue(solver.resolveTest(cls[i], cls[1]).getLiterals()
					.equals(cls[i].getLiterals()));
		}

		for (int i = 1; i <= 2; i++) {
			assertTrue(solver.resolveTest(cls[i], cls[3]).getLiterals()
					.equals(cls[0].getLiterals()));
			assertTrue(solver.resolveTest(cls[3], cls[i]).getLiterals()
					.equals(cls[0].getLiterals()));
		}

		assertTrue(solver.resolveTest(cls[4], cls[5]).getLiterals()
				.equals(cls[7].getLiterals()));
		assertTrue(solver.resolveTest(cls[5], cls[4]).getLiterals()
				.equals(cls[7].getLiterals()));
		assertTrue(solver.resolveTest(cls[4], cls[6]).getLiterals()
				.equals(cls[1].getLiterals()));
		assertTrue(solver.resolveTest(cls[6], cls[4]).getLiterals()
				.equals(cls[1].getLiterals()));

		Clause[] cl = new Clause[4];
		cl[0] = new Clause(-3, 4, 2, 19, -5, 12, 31, -2, 7);
		cl[1] = new Clause(14, -4, 12, -7, 17, -2);
		cl[2] = new Clause(-3, 19, -5, 12, 31, 14, 17);
		cl[3] = new Clause(14, 12, 17, -3, 19, -5, 31);

		assertTrue(solver.resolveTest(cl[0], cl[1]).getLiterals()
				.equals(cl[2].getLiterals()));
		assertTrue(solver.resolveTest(cl[1], cl[0]).getLiterals()
				.equals(cl[3].getLiterals()));
	}

	@Test
	public void testGetNextVarAndLearn() {
		Vector<Clause> cls = new Vector<Clause>();
		cls.add(new Clause());
		cls.add(new Clause(1));
		cls.add(new Clause(-1, 2));
		cls.add(new Clause(2, 3));
		cls.add(new Clause(-1, 3, 4));
		cls.add(new Clause(1, 2));
		ClauseSet clauses = new ClauseSet(cls);
		CDCL solver = new CDCL(clauses);

		assertEquals(solver.instance.getVariables().get(1).getActivity(), 4.0,
				0.0001);
		assertEquals(solver.instance.getVariables().get(2).getActivity(), 3.0,
				0.0001);
		assertEquals(solver.instance.getVariables().get(3).getActivity(), 2.0,
				0.0001);
		assertEquals(solver.instance.getVariables().get(4).getActivity(), 1.0,
				0.0001);
		assertEquals(solver.getNextVarTest().getId(), 1);

		clauses.learn(new Clause(2, -3));
		clauses.learn(new Clause(-2, 4));
		clauses.learn(new Clause(2, 3, 4));
		clauses.learn(new Clause(-2, 3, -4));

		assertEquals(solver.instance.getVariables().get(1).getActivity(),
				4.0 * 0.95, 0.0001);
		assertEquals(solver.instance.getVariables().get(2).getActivity(), 3.0
				* 0.95 * 1.1 * 1.1 * 1.1 * 1.1, 0.0001);
		assertEquals(solver.instance.getVariables().get(3).getActivity(), 2.0
				* 0.95 * 1.1 * 1.1 * 1.1, 0.0001);
		assertEquals(solver.instance.getVariables().get(4).getActivity(), 1.0
				* 0.95 * 1.1 * 1.1 * 1.1, 0.0001);
		assertEquals(solver.getNextVarTest().getId(), 2);
		assertEquals(solver.instance.getClauses().size(), 10);
		for (Clause c : solver.instance.getClauses())
			assertNotNull(c);
	}

	@Test
	public void testSolve() {
		File f = null;
		Vector<File> paths;
		CDCL solver;

		f = new File("no");
		paths = new Vector<File>(Arrays.asList(f.listFiles()));
		paths.add(new File("aim-100-1_6-no-1.cnf"));
		paths.add(new File("formula01.cnf"));
		//paths.add(new File("barrel5-no.cnf"));
		//paths.add(new File("hole8-no.cnf"));
		//paths.add(new File("longmult6-no.cnf"));
		//working but takes about 15min to calculate
		boolean sat;
		for (File path : paths) {
			System.out.println(path);
			solver = new CDCL(path.toString());
			assertFalse(solver.solve());
			sat = true;
			for (Clause c : solver.instance.getClauses())
				if (!c.getClauseState(solver.instance.getVariables()).equals(
						Clause.ClauseState.SAT))
					sat = false;
			assertFalse(sat);
		}

		f = new File("yes");
		paths = new Vector<File>(Arrays.asList(f.listFiles()));
		paths.add(new File("aim-200-2_0-yes1-2.cnf"));
		paths.add(new File("formula02.cnf"));
		//paths.add(new File("hanoi4-yes.cnf"));
		paths.add(new File("ssa7552-160-yes.cnf"));
		for (File path : paths) {
			System.out.println(path);
			solver = new CDCL(path.toString());
			assertTrue(solver.solve());
			for (Clause c : solver.instance.getClauses())
				assertEquals(c.getClauseState(solver.instance.getVariables()),
						Clause.ClauseState.SAT);
		}


	}

	@Test
	public void testUnitPropagation() {
		Stack<Variable> stack = new Stack<Variable>();
		ClauseSet cls = new ClauseSet("formula01.cnf");
		Clause empty = cls.unitPropagation(0, stack);
		assertNotNull(empty);
		for (int index = 0; index < stack.size(); index++) {
			Variable var = stack.elementAt(index);
			assertEquals(var.getId(), index + 1);
			assertEquals(var.getLevel(), 0);
			assertNotNull(var.getReason());
			assertNotEquals(var.getState(), Variable.State.OPEN);
		}

		for (Integer lit : empty.getLiterals()) {
			Variable.State st = Variable.State.OPEN;
			for (Variable var : stack)
				if (var.getId() == Math.abs(lit))
					st = var.getState();
			assertNotEquals(st, Variable.State.OPEN);
		}
		stack.clear();

		cls = new ClauseSet("formula02.cnf");
		assertNull(cls.unitPropagation(1, stack));
		int[] vars = { 1, 2, 4, 5, 3 };
		int index = 0;
		while (!stack.isEmpty()) {
			Variable var = stack.elementAt(0);
			stack.remove(0);
			assertEquals(var.getId(), vars[index]);
			assertEquals(var.getLevel(), 1);
			assertNotNull(var.getReason());
			assertNotEquals(var.getState(), Variable.State.OPEN);
			index++;
		}
	}

	@Test
	public void testBacktrackAndClauseState() {
		CDCL solver = new CDCL("formula01.cnf");
		for (int i = 0; i < 2; i++) {
			if (i == 1)
				solver = new CDCL("formula02.cnf");
			solver.instance.unitPropagation(1, solver.getStack());
			solver.backtrackTest(0);
			assertTrue(solver.getStack().isEmpty());
			for (Integer id : solver.instance.getVariables().keySet()) {
				Variable var = solver.instance.getVariables().get(id);
				assertEquals(var.getState(), Variable.State.OPEN);
				assertNull(var.getReason());
				assertEquals(var.getLevel(), Integer.MIN_VALUE);
				assertEquals(solver.instance.getUnits().size(), 1);
			}
			solver.instance.unitPropagation(0, solver.getStack());
			solver.backtrackTest(0);
			assertEquals(solver.getStack().size(), solver.instance
					.getVariables().size());
		}

		Vector<Clause> cls = new Vector<Clause>();
		cls.add(new Clause(1, 3));
		cls.add(new Clause(-1, 4));
		cls.add(new Clause(5, 7));
		cls.add(new Clause(-5, 3));
		cls.add(new Clause(-3, -5));
		ClauseSet cs = new ClauseSet(cls);
		solver = new CDCL(cs);
		for (Clause c : cs.getClauses())
			assertEquals(c.getClauseState(solver.instance.getVariables()),
					Clause.ClauseState.SUCCESS);

		assertNull(solver.instance
				.getVariables()
				.get(1)
				.assign(true, solver.instance.getVariables(),
						solver.instance.getUnits(), null, 1));
		solver.getStack().push(solver.instance.getVariables().get(1));
		assertEquals(
				cs.getClauses().get(0)
				.getClauseState(solver.instance.getVariables()),
				Clause.ClauseState.SAT);
		assertEquals(
				cs.getClauses().get(1)
				.getClauseState(solver.instance.getVariables()),
				Clause.ClauseState.UNIT);

		assertNull(solver.instance.unitPropagation(1, solver.getStack()));
		assertNull(solver.instance
				.getVariables()
				.get(5)
				.assign(true, solver.instance.getVariables(),
						solver.instance.getUnits(), null, 2));
		solver.getStack().push(solver.instance.getVariables().get(5));
		assertEquals(
				cs.getClauses().get(2)
				.getClauseState(solver.instance.getVariables()),
				Clause.ClauseState.SAT);
		assertEquals(
				cs.getClauses().get(3)
				.getClauseState(solver.instance.getVariables()),
				Clause.ClauseState.UNIT);

		assertNotNull(solver.instance.unitPropagation(2, solver.getStack()));
		assertEquals(
				cs.getClauses().get(4)
				.getClauseState(solver.instance.getVariables()),
				Clause.ClauseState.EMPTY);

		solver.backtrackTest(1);
		assertFalse(solver.getStack().contains(
				solver.instance.getVariables().get(5)));
		assertFalse(solver.getStack().contains(
				solver.instance.getVariables().get(3)));
		assertFalse(solver.getStack().contains(
				solver.instance.getVariables().get(7)));
		assertTrue(solver.getStack().contains(
				solver.instance.getVariables().get(1)));
		assertTrue(solver.getStack().contains(
				solver.instance.getVariables().get(4)));
		assertEquals(solver.getStack().size(), 2);
		for (int i = 3; i <= 7; i += 2) {
			assertEquals(solver.instance.getVariables().get(i).getLevel(),
					Integer.MIN_VALUE);
			assertNull(solver.instance.getVariables().get(i).getReason());
			assertEquals(solver.instance.getVariables().get(i).getState(),
					Variable.State.OPEN);
		}
		for (int i = 1; i <= 4; i += 3) {
			assertEquals(solver.instance.getVariables().get(i).getLevel(), 1);
			assertEquals(solver.instance.getVariables().get(i).getState(),
					Variable.State.TRUE);
		}
	}

	@Test
	public void testGet1UIP() {
		Vector<Clause> cls = new Vector<Clause>();
		cls.add(new Clause(1, 3));
		cls.add(new Clause(-1, 4));
		cls.add(new Clause(5, 7));
		cls.add(new Clause(-5, 3));
		cls.add(new Clause(-3, -5));
		ClauseSet cs = new ClauseSet(cls);
		CDCL solver = new CDCL(cs);
		assertNull(solver.instance
				.getVariables()
				.get(1)
				.assign(true, solver.instance.getVariables(),
						solver.instance.getUnits(), null, 1));
		solver.getStack().push(solver.instance.getVariables().get(1));

		assertNull(solver.instance.unitPropagation(1, solver.getStack()));
		assertNull(solver.instance
				.getVariables()
				.get(5)
				.assign(true, solver.instance.getVariables(),
						solver.instance.getUnits(), null, 2));
		solver.getStack().push(solver.instance.getVariables().get(5));

		assertNotNull(solver.instance.unitPropagation(2, solver.getStack()));
		assertEquals(
				cs.getClauses().get(4)
				.getClauseState(solver.instance.getVariables()),
				Clause.ClauseState.EMPTY);

		Clause UIP = solver.get1UIPTest(cs.getClauses().get(4), 
				solver.getStack().peek().getReason());
		Clause UIPTest = solver.resolveTest(new Clause(-5,3),new Clause(-3,-5));
		assertEquals(UIP.getLiterals(), new Clause(-5).getLiterals());
		assertEquals(UIP.getLiterals(), UIPTest.getLiterals());
	}

	@Test
	public void testAnalyseConflict() {
		Vector<Clause> cls = new Vector<Clause>();
		cls.add(new Clause(1, 3));
		cls.add(new Clause(-1, 4));
		cls.add(new Clause(5, 7));
		cls.add(new Clause(-5, 3));
		cls.add(new Clause(-3, -5));
		ClauseSet cs = new ClauseSet(cls);
		CDCL solver = new CDCL(cs);
		int level = solver.analyseConflictTest(cs.getClauses().get(4));
		assertEquals(level, -1);

		assertNull(solver.instance
				.getVariables()
				.get(1)
				.assign(true, solver.instance.getVariables(),
						solver.instance.getUnits(), null, 1));
		solver.getStack().push(solver.instance.getVariables().get(1));

		assertNull(solver.instance.unitPropagation(1, solver.getStack()));
		assertNull(solver.instance
				.getVariables()
				.get(5)
				.assign(true, solver.instance.getVariables(),
						solver.instance.getUnits(), null, 2));
		solver.getStack().push(solver.instance.getVariables().get(5));

		assertNotNull(solver.instance.unitPropagation(2, solver.getStack()));
		assertEquals(
				cs.getClauses().get(4)
				.getClauseState(solver.instance.getVariables()),
				Clause.ClauseState.EMPTY);
		int numClauses = solver.instance.getClauses().size();

		level = solver.analyseConflictTest(cs.getClauses().get(4));
		assertEquals(level, 0);
		assertEquals(solver.instance.getClauses().size(), numClauses+1);
		assertEquals(solver.instance.getClauses().elementAt(5).getLiterals(),
				new Clause(-5).getLiterals());
	}
}
