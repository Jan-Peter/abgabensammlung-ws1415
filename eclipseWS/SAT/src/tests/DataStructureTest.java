package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
//import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Vector;

import org.junit.Test;

import dataStructure.Clause;
import dataStructure.Clause.ClauseState;
import dataStructure.ClauseSet;

public class DataStructureTest {

	//@Test
	public void test() {
		// Test ClauseSets
		Vector<ClauseSet> formula = new Vector<ClauseSet>();
		formula.add(new ClauseSet("formula01.cnf"));
		formula.add(new ClauseSet("formula02.cnf"));
		formula.add(new ClauseSet("aim-100-1_6-no-1.cnf"));
		formula.add(new ClauseSet("aim-200-2_0-yes1-2.cnf"));
		formula.add(new ClauseSet("hole8-no.cnf"));
		formula.add(new ClauseSet("hanoi4-yes.cnf"));
		for (ClauseSet f : formula) {
			assertNotNull(f);
		}

		Vector<Integer> literals = new Vector<Integer>();
		literals.add(new Integer(2));
		literals.add(new Integer(3));
		literals.add(new Integer(-4));
		//assertTrue(myClauseEqual(new Clause(literals, null), formula.get(0).unitPropagation()));
		literals.clear();
		//assertEquals(null, formula.get(1).unitPropagation());
		
		
		// Test Clauses
		Vector<Clause> clauses = new Vector<Clause>();
		for (int i = 1; i < 100; i++) {
			literals.add((int) (Math.pow(-1, i) * i));
		}
		clauses.add(new Clause(literals, null));
		literals.clear();
		literals.add(1);
		clauses.add(new Clause(literals, null));
		for (Clause c : clauses) {
			assertNotNull(c);
			assertEquals(c.getLiterals().size(), c.size());
			assertEquals(c.getLiterals().size(), c.size());
			assertFalse(c.getPolarity(3));
			assertFalse(c.getPolarity(100));
		}
		
		ClauseSet f=formula.get(2);
		//assertNotEquals(0, f.getClauses().get(0).getUnassigned(f.getVariables()));
		assertNotSame(0, f.getClauses().get(0).getUnassigned(f.getVariables()));
		f=formula.get(1);
		assertEquals(0, f.getClauses().get(0).getUnassigned(f.getVariables()));
		assertTrue(stateEqual(Clause.ClauseState.EMPTY,f.getClauses().get(0).initWatch(f.getVariables())));
	}

	private boolean stateEqual(ClauseState unit, ClauseState initWatch) {
		return unit.equals(initWatch);
		
	}

	/**
	 * checks whether two clauses contain the same variables
	 * @param clause1
	 * @param clause2
	 * @return
	 */
	private boolean myClauseEqual(Clause clause1, Clause clause2) {
		return clause1.getLiterals().equals(clause2.getLiterals());
	}

}
