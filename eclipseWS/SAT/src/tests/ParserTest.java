package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import parser.Parser;

public class ParserTest {

	@Test
	public void test() {
		// Parser is tested
		String s = "goldb-heqc-k2mul.cnf";
		Parser p = new Parser(s);

		// Tests
		//p should not be null because the path is right
		assertEquals(true, p!=null);
		assertEquals(11680, p.stateVars());
		assertEquals(74581, p.stateClauses());
		assertEquals(p.stateClauses(), p.getNumClauses());
		assertEquals(p.stateVars(), p.getNumVars());
		assertEquals(s, p.getFilename());
		assertEquals(3438, p.getMaximumNoOfOccurances());
		assertEquals(37, (int) p.getMaximumOccuringVar().get(0));
		assertEquals(355, (int) p.getUnitClauses().get(0));
		assertEquals(356, (int) p.getUnitClauses().get(1));
		assertEquals(6107, (int) p.getUnitClauses().get(2));
		assertEquals(6108, (int) p.getUnitClauses().get(3));
	}

}
