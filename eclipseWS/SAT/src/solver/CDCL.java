package solver;

import java.util.Map.Entry;
import java.util.Stack;
import java.util.Vector;

import dataStructure.*;

public class CDCL {
	public ClauseSet instance;
	private Stack<Variable> stack;
	
	// Test only
	public CDCL() {
		
	}

	public CDCL(String filename) {
		instance = new ClauseSet(filename);
		stack = new Stack<Variable>();
	}
	
	// Test only
	public CDCL(ClauseSet clauses) {
		instance = clauses;
		stack = new Stack<Variable>();
	}
	
	// Test only
	public Stack<Variable> getStack() {
		return stack;
	}

	/**
	 * resolve c1 and c2
	 * 
	 * @param c1
	 * @param c2
	 * @return the resolved Clause
	 */
	private Clause resolve(Clause c1, Clause c2) {
		assert(c1 != null && c2 != null);
		Vector<Integer> litsResolved = new Vector<Integer>();
		Vector<Integer> litsOmitted = new Vector<Integer>();
		for (Integer lit : c1.getLiterals()) {
			if (!c2.getLiterals().contains(-lit) && !litsResolved.contains(lit) 
					&& !c1.getLiterals().contains(-lit))
				litsResolved.add(lit);
			else {
				litsOmitted.add(-lit);
				litsOmitted.add(lit);
			}
		}
		for (Integer lit : c2.getLiterals())
			if (!litsResolved.contains(lit) && !c2.getLiterals().contains(-lit) 
					&& !litsOmitted.contains(lit))
				litsResolved.add(lit);
		return new Clause(litsResolved);
	}
	
	// Test only
	public Clause resolveTest(Clause c1, Clause c2) {
		return resolve(c1, c2);
	}

	/**
	 * Get the 1UIP-Clause for:
	 * 
	 * @param conflict
	 * @param reason
	 * @return 1UIP CLause
	 */
	private Clause get1UIP(Clause conflict, Clause reason) {
		Clause newClause = resolve(conflict, reason);
		Vector<Integer> maxLevelVars = getMaxLevelVariables(newClause);
		while (maxLevelVars.size() > 1) {
			Clause c = null;
			do {
				Integer currLit = maxLevelVars.firstElement();
				c = instance.getVariables().get(Math.abs(currLit)).getReason();
				maxLevelVars.removeElement(currLit);
			} while (c == null);
			assert(c != null);
			newClause = resolve(newClause, c);
			maxLevelVars = getMaxLevelVariables(newClause);
		}
		return newClause;
	}
	
	// Test only
	public Clause get1UIPTest(Clause conflict, Clause reason) {
		return get1UIP(conflict, reason);
	}

	/**
	 * @param c
	 * @return the Literals in c with the highest Level
	 */
	private Vector<Integer> getMaxLevelVariables(Clause c) {
		int maxLevel = stack.peek().getLevel();
		Vector<Integer> maxLevelVars = new Vector<Integer>();
		for (Integer lit : c.getLiterals())
			if (instance.getVariables().get(Math.abs(lit)).getLevel() == maxLevel)
				maxLevelVars.add(lit);
		return maxLevelVars;
	}

	/**
	 * Analyzes the conflict to get the level to backtrack to and learns a new
	 * Clause (the 1UIP)
	 * 
	 * @param conflict
	 * @return the level to backtrack to or -1 if there are no more levels
	 */
	private int analyseConflict(Clause conflict) {
		if(stack.isEmpty())
			return -1;
		int level = stack.peek().getLevel();
		if(level == 0)
			return -1;
		Clause firstUIP = get1UIP(conflict, stack.peek().getReason());
		instance.learn(firstUIP);
		return firstUIP.getMaxLevel(instance.getVariables(), level);
	}
	
	// Test only
	public int analyseConflictTest(Clause conflict) {
		return analyseConflict(conflict);
	}

	/**
	 * 
	 * @return the free variable with highest activity
	 */
	private Variable getNextVar() {
		Variable mostActive = null;
		for (Entry<Integer, Variable> e : instance.getVariables().entrySet()) {
			if (e.getValue().getState() == Variable.State.OPEN && 
					(mostActive == null
					|| mostActive.getActivity() < e.getValue().getActivity())) {
				mostActive = e.getValue();
			}
			e.getValue().decreaseActivity();
		}
		return mostActive;
	}
	
	// Test only
	public Variable getNextVarTest() {
		return getNextVar();
	}

	/**
	 * runs the CDCL - Algorithm
	 * 
	 * @return true (i.e. SAT) or false (i.e. UNSAT)
	 */
	public boolean solve() {
		int level = 0;
		stack.clear(); // nur zur Sicherheit bei Falschaufrufen
		while (true) {			
			Clause ec = instance.unitPropagation(level, stack);
			if (ec != null) {
				level = analyseConflict(ec);
				if (level == -1)
					return false;
				backtrack(level);
			} else {
				if(instance.isSat())
					return true;
				level++;
				Variable v=getNextVar();
				assert(v != null);
				v.assign(false, instance.getVariables(), instance.getUnits(), null, level);
				stack.push(v);
			}
		}
	}

	/**
	 * delete variables from stack until the desired level is reached
	 * 
	 * @param level
	 */
	private void backtrack(int level) {
		while (!stack.empty()) {
			Variable var = stack.pop();
			if (var.getLevel() <= level) {
				stack.push(var);
				break;
			}
			var.reOpen();
			stack.remove(var);
		}
		instance.resetUnits();
	}
	
	public void backtrackTest(int level) {
		backtrack(level);
	}
}
