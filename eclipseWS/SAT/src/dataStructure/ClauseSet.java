package dataStructure;

import java.util.HashMap;
import java.util.Stack;
import java.util.Vector;

import parser.Parser;

/**
 * A set of clauses.
 * 
 */
public class ClauseSet {
	/* Number of variables */
	private int varNum;

	/* Clauses of this set */
	private Vector<Clause> clauses;

	/* List of all variables */
	private HashMap<Integer, Variable> variables;
	
	/* List of all units */
	private Vector<Clause> units;

	/**
	 * Constructs a clause set from the given DIMACS file.
	 * 
	 * @param filePath
	 *            file path of the DIMACS file.
	 */
	public ClauseSet(String filePath) {
		Parser clauseSet = new Parser(filePath);
		this.varNum = clauseSet.getNumVars();
		Vector<Vector<Integer>> parsedClauses = clauseSet.getClauses();
		this.variables = new HashMap<Integer, Variable>(varNum);
		this.clauses = new Vector<Clause>();
		parsedClauses=checkForTautology(parsedClauses);
		for(int i=0; i<parsedClauses.size(); i++) {
			for (int j = 0; j < parsedClauses.get(i).size(); j++) {
				// the key is always the positive value of the literal
				Integer key = Math.abs(parsedClauses.get(i).get(j));
				if (!variables.containsKey(key))
					variables.put(key, new Variable(key));
				else
					variables.get(key).increaseActivityByOne();
			}
		}
		// initialize clauses and units
		this.units = new Vector<Clause>();
		for(int i=0; i<parsedClauses.size(); i++) {
			Clause c = new Clause(parsedClauses.get(i), variables);
			if(c.initWatch(variables).equals(Clause.ClauseState.UNIT))
				units.add(c);			
			clauses.add(c);
		}
	}
	
	private Vector<Vector<Integer>> checkForTautology(
			Vector<Vector<Integer>> parsedClauses) {
		Vector<Integer> remove=new Vector<Integer>();
		for(int k=0; k < parsedClauses.size(); k++){
			Vector<Integer> clause=parsedClauses.get(k);
			for(int i=0; i< clause.size(); i++)
				for(int j=0; j<i; j++)
					if(clause.get(i)==-clause.get(j))
						remove.add(k);
		}
		//System.out.println(remove);
		for(int i=0; i<remove.size();i++){
			parsedClauses.remove((int) remove.get(i)-i);
		}
		return parsedClauses;
	}

	// Test only
	public ClauseSet(Vector<Clause> cls) {
		Vector<Integer> vars = new Vector<Integer>();
		for(Clause c : cls)
			for(Integer lit : c.getLiterals())
				if(!vars.contains(Math.abs(lit)))
					vars.add(Math.abs(lit));
		this.varNum = vars.size();
		this.variables = new HashMap<Integer, Variable>(varNum);
		for(Clause c : cls)
			for(Integer lit : c.getLiterals()) {
				Integer key = Math.abs(lit);
				if(!variables.containsKey(key))
					variables.put(key, new Variable(key));
				else
					variables.get(key).increaseActivityByOne();
			}
		// initialize clauses and units
		this.units = new Vector<Clause>();
		this.clauses = new Vector<Clause>();
		for(Clause c : cls) {
			Clause cn = new Clause(c.getLiterals(), variables);
			if(cn.initWatch(variables).equals(Clause.ClauseState.UNIT))
				units.add(cn);
			clauses.add(cn);
		}
	}

	/**
	 * Executes unit propagation and checks for the existence of an empty
	 * clause.
	 * 
	 * @return null, if no empty clause exists, otherwise an empty clause.
	 */
	public Clause unitPropagation(int currentLevel, Stack<Variable> stack) {
		while(!units.isEmpty()) {
			Clause c = units.firstElement();
			units.remove(0);
			int lit = c.getUnassigned(variables);
			if(lit == 0)
				continue;
			Variable v = variables.get(Math.abs(lit));
			Clause res = v.assign(c.getPolarity(Math.abs(lit)), variables, 
					units, c, currentLevel);
			stack.push(v);
			if(res != null)
				return res;
		}
		return null;
	}

	@Override
	public String toString() {
		return clausesToString() + "\n\n" + varsToString();
	}

	/**
	 * Returns all clauses as string representation.
	 * 
	 * @return a string representation of all clauses.
	 */
	public String clausesToString() {
		String res = "";
		for (Clause clause : clauses)
			res += clause + "\n";
		return res;
	}

	/**
	 * Returns all variables as string representation.
	 * 
	 * @return a string representation of all variables.
	 */
	public String varsToString() {
		String res = "";
		for (int i = 1; i <= varNum; i++)
			res += "Variable " + i + ": " + variables.get(i) + "\n\n";
		return res;
	}

	public Vector<Clause> getClauses() {
		return clauses;
	}

	public HashMap<Integer, Variable> getVariables() {
		return variables;
	}

	/**
	 * add a new Clause and initialize it
	 * @param newClause
	 */
	public void learn(Clause newClause) {
		newClause.addClauseToVariables(variables);
		clauses.add(newClause);
		for(Integer lit : newClause.getLiterals())
			variables.get(Math.abs(lit)).increaseActivity();
	}

	public Vector<Clause> getUnits() {
		return units;
	}

	public void resetUnits() {
		this.units = new Vector<Clause>();
		for(Clause c : clauses)
			if(c.initWatch(variables).equals(Clause.ClauseState.UNIT))
				units.add(c);
	}
	
	public boolean isSat() {
		for (Clause c : clauses)
			if (!c.isSat(variables))
				return false;
		return true;
	}
}
