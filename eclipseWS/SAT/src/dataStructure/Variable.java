package dataStructure;

import java.util.HashMap;
import java.util.Vector;

import dataStructure.Clause.ClauseState;


/**
 * A variable.
 * 
 */
public class Variable {

	/* Assignment states of a variable */
	public enum State {
		TRUE, FALSE, OPEN
	};

	/* Current assignment */
	private State state;

	/* Variable ID (range from 1 to n) */
	private int id;

	/* Clauses containing this variable */
	private Vector<Clause> watched;
	
	private float activity;

	private Clause reason;
	private int level;

	/**
	 * Creates a variable with the given ID.
	 * 
	 * @param id
	 *            ID of the variable
	 */
	public Variable(int id) {
		this.id = id;
		this.state = State.OPEN;
		this.watched = new Vector<Clause>();
		this.activity = 1;
		this.reason = null;
		this.level = Integer.MIN_VALUE;
	}

	/**
	 * Adds watched Clause
	 * 
	 * @param c 
	 * 			  Clause containing this Variable
	 */
	public void addClause(Clause c){
		if(!watched.contains(c))
			watched.add(c);
	}
	
	/**
	 * Returns the current assignment state of this variable.
	 * 
	 * @return current assignment state
	 */
	public State getState() {
		return state;
	}

	/**
	 * Returns the ID of this variable.
	 * 
	 * @return ID of this variable
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @return the activity
	 */
	public float getActivity() {
		return activity;
	}
	
	/**
	 * @return the reason
	 */
	public Clause getReason() {
		return reason;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * increase Activity of this variable
	 */
	protected void increaseActivity() {
		this.activity = this.activity * 1.1f;
	}

	/**
	 * decrease Activity of this variable
	 */
	public void decreaseActivity() {
		this.activity = this.activity * 0.95f;
	}

	/**
	 * For initialization only
	 */
	public void increaseActivityByOne() {
		this.activity++;
	}
	
	/**
	 * Assigns variable with the given value and updates the internal state of
	 * the corresponding clauses.
	 * 
	 * @param val 
	 * 			  value to be assigned
	 * @param variables
	 * 			  map of variables
	 * @param units
	 * 			  list of unit clauses
	 * @return
	 */
	public Clause assign(boolean val, HashMap<Integer, Variable> variables,
			Vector<Clause> units, Clause reason, int level) {
		if(this.state.equals(State.OPEN)){
			this.state = val ? State.TRUE : State.FALSE;
			this.level = level;
			this.reason = reason;
			for (Clause c : watched) {
				boolean pol = c.getPolarity(id);
				if(pol != val) {				
					ClauseState st = c.reWatch(variables, id);
					if(st.equals(ClauseState.EMPTY))
						return c;
					else if(st.equals(ClauseState.UNIT))
						units.add(c);
				}
			}
		}
		return null;
	}

	@Override
	public String toString() {
		String res = "[" + id + ": " + state + " ";
		res += "\n\tAdjacence List: " + watched + "\n\tActivity: " + activity +
				"\n\treason: " + reason + "\n\tlevel: " + level;
		return res + "\n]";
	}
	
	/**
	 * deletes the decision / unit propagation made for this variable (called while backtracking)
	 */
	public void reOpen() {
		this.state = State.OPEN;
		this.reason = null;
		this.level = Integer.MIN_VALUE;
	}
}