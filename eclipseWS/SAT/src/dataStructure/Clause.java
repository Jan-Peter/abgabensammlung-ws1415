package dataStructure;

import java.util.HashMap;
import java.util.Vector;

/**
 * A clause.
 * 
 */
public class Clause {

	public enum ClauseState {
		SAT, EMPTY, UNIT, SUCCESS
	};

	/* Literals of the clause */
	private Vector<Integer> literals;

	/* Watched literals */
	private int lit1, lit2;

	/**
	 * Creates a new clause with the given literals.
	 * 
	 * @param literals
	 *            literals of the clause
	 * @param variables
	 *            or null if no variables are assigned yet
	 */
	public Clause(Vector<Integer> literals, HashMap<Integer, Variable> variables) {
		this.literals = literals;
		if (variables != null)
			for (Integer lit : literals)
				variables.get(Math.abs(lit)).addClause(this);
	}

	public Clause(Vector<Integer> literals) {
		this.literals = literals;
	}

	// Test only
	public Clause(Integer... lits) {
		this.literals = new Vector<Integer>();
		for(Integer i : lits)
			literals.add(i);
	}

	public void addClauseToVariables(HashMap<Integer, Variable> variables) {
		if(variables != null)
			for(Integer lit : literals)
				variables.get(Math.abs(lit)).addClause(this);
	}

	/**
	 * Returns the literals of this clause.
	 * 
	 * @return literals of this clause
	 */
	public Vector<Integer> getLiterals() {
		return literals;
	}

	public Vector<Integer> getWatchedLiterals() {
		Vector<Integer> lits = new Vector<Integer>();
		lits.add(lit1);
		lits.add(lit2);
		return lits;
	}

	/**
	 * Returns an unassigned literal of this clause.
	 * 
	 * @param variables
	 *            variable objects
	 * @return an unassigned literal, if one is watched, 0 otherwise
	 */
	public int getUnassigned(HashMap<Integer, Variable> variables) {
		if (variables.get(Math.abs(lit1)).getState()
				.equals(Variable.State.OPEN))
			return lit1;
		if (variables.get(Math.abs(lit2)).getState()
				.equals(Variable.State.OPEN))
			return lit2;
		return 0;
	}

	/**
	 * Returns the phase of the variable within this clause.
	 * 
	 * @param num
	 *            variable ID (>= 1)
	 * @return true, if variable is positive within this clause, otherwise false
	 */
	public boolean getPolarity(int num) {
		return literals.contains(num);
	}

	/**
	 * Returns the size of this clause.
	 * 
	 * @return size of this clause.
	 */
	public int size() {
		return literals.size();
	}

	/**
	 * initialize watched literals
	 * 
	 * @param variables
	 *            map of variables
	 * @return status of Clause (EMTPY or UNIT) or SUCCESS, if successful
	 */
	public ClauseState initWatch(HashMap<Integer, Variable> variables) {
		if (literals.size() == 0)
			return Clause.ClauseState.EMPTY;
		if(isSat(variables))
			return Clause.ClauseState.SAT;	// TODO: added
		lit1 = lit2 = 0;
		for (Integer lit : literals) {
			if (variables.get(Math.abs(lit)).getState()
					.equals(Variable.State.OPEN)) {
				if (lit1 == 0)
					lit1 = lit;
				else if (lit2 == 0) {
					lit2 = lit;
					return Clause.ClauseState.SUCCESS;
				}
			}
		}
		if (lit1 == 0)
			return Clause.ClauseState.EMPTY;
		lit2 = lit1; // hier: lit2 = 0;
		return Clause.ClauseState.UNIT;
	}

	/**
	 * search for a new watched literal
	 * 
	 * @param variables
	 *            map of variables
	 * @param lit
	 *            literal to free
	 * @return status of Clause (EMPTY, UNIT or SAT) or SUCCESS, if successful
	 */
	protected ClauseState reWatch(HashMap<Integer, Variable> variables, int lit) {
		//Pick the right watched lit
		int lit_tmp1 = 0, lit_tmp2 = 0;
		if (Math.abs(lit1) == Math.abs(lit)) {
			lit_tmp1 = lit1;
			lit_tmp2 = lit2;
		} else if (Math.abs(lit2) == Math.abs(lit)) {
			lit_tmp1 = lit2;
			lit_tmp2 = lit1;
		} else
			return ClauseState.SUCCESS;

		for (Integer l : literals) {
			if ((l != lit_tmp1) && (l != lit_tmp2)) {
				Variable.State st = variables.get(Math.abs(l)).getState();
				boolean pol = getPolarity(Math.abs(l));
				if (st.equals(Variable.State.OPEN)
						|| (pol && st.equals(Variable.State.TRUE) || (!pol && st
								.equals(Variable.State.FALSE)))) {
					lit1 = l;
					lit2 = lit_tmp2;
					return ClauseState.SUCCESS;
				}
			}
		}
		Variable.State st = variables.get(Math.abs(lit_tmp2)).getState();
		boolean pol = getPolarity(Math.abs(lit_tmp2));
		if ((pol && st.equals(Variable.State.TRUE))
				|| (!pol && st.equals(Variable.State.FALSE)))
			return ClauseState.SAT;
		// folgendes evtl unnötig???
		Variable.State st2 = variables.get(Math.abs(lit_tmp1)).getState();
		boolean pol2 = getPolarity(Math.abs(lit_tmp1));
		if ((pol2 && st2.equals(Variable.State.TRUE))
				|| (!pol2 && st2.equals(Variable.State.FALSE)))
			return ClauseState.SAT;
		if (st.equals(Variable.State.OPEN)) {
			lit1 = lit_tmp2;	// TODO: hinzugefügt, passts?
			lit2 = lit_tmp2;	// TODO
			return ClauseState.UNIT;
		}
		lit1 = lit_tmp2;		// TODO
		lit2 = lit_tmp2;		// TODO
		return ClauseState.EMPTY;
	}

	@Override
	public String toString() {
		String res = "{ ";
		for (Integer i : literals)
			res += i + " ";
		return res + "}" + " watched: " + lit1 + ", " + lit2;
	}

	public int getMaxLevel(HashMap<Integer, Variable> variables, int level) {
		int maxLevel=0;
		for(Integer lit : literals){
			int newLevel = variables.get(Math.abs(lit)).getLevel();
			if(maxLevel < newLevel && newLevel < level)
				maxLevel = newLevel;
		}
		return maxLevel;
	}

	public boolean isSat(HashMap<Integer, Variable> variables) {
		return isSat(variables, lit1) || isSat(variables, lit2);
	}

	private boolean isSat(HashMap<Integer, Variable> variables, int lit) {
		return (lit > 0 && variables.get(lit).getState()
				.equals(Variable.State.TRUE)) ||
				(lit < 0 && variables.get(Math.abs(lit)).getState()
						.equals(Variable.State.FALSE));
	}

	public ClauseState getClauseState(HashMap<Integer, Variable> variables) {
		assert(lit1 != 0 && lit2 != 0);
		if(isSat(variables))
			return ClauseState.SAT;
		if(lit1 == lit2) {
			Variable.State st = variables.get(Math.abs(lit1)).getState();
			if(lit1 > 0 && st.equals(Variable.State.FALSE) ||
					lit1 < 0 && st.equals(Variable.State.TRUE))
				return ClauseState.EMPTY;
			if(st.equals(Variable.State.OPEN))
				return ClauseState.UNIT;
		}
		return ClauseState.SUCCESS;
	}
}